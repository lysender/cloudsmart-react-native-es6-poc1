import React from 'react';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import { LoginScreen } from "../screens/LoginScreen";
import { AuthLoadingScreen } from "../screens/AuthLoadingScreen";

export default createAppContainer(createSwitchNavigator(
  {
    // You could add another route here for authentication.
    // Read more at https://reactnavigation.org/docs/en/auth-flow.html
    AuthLoading: AuthLoadingScreen,
    Auth: LoginScreen,
    App: MainTabNavigator,
  },
  {
    initialRouteName: 'AuthLoading'
  }
));
