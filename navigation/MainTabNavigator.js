import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import { ExploreScreen } from "../screens/ExploreScreen";
import { MyRepoDetailScreen } from "../screens/MyRepoDetailScreen";
import { AccountScreen } from '../screens/AccountScreen';
import { RepoDetailScreen } from "../screens/RepoDetailScreen";

// Home stack
import { MyForksScreen } from "../screens/HomeStacks/MyForksScreen";
import { MyIssuesScreen } from "../screens/HomeStacks/MyIssuesScreen";
import { MyPullRequestsScreen } from "../screens/HomeStacks/MyPullRequestsScreen";
import { MyReleasesScreen } from "../screens/HomeStacks/MyReleasesScreen";
import { MyWatchersScreen } from "../screens/HomeStacks/MyWatchersScreen";
import { MyStarsScreen } from "../screens/HomeStacks/MyStarsScreen";

// Explore stack
import { ForksScreen } from "../screens/ExploreStacks/ForksScreen";
import { IssuesScreen } from "../screens/ExploreStacks/IssuesScreen";
import { PullRequestsScreen } from "../screens/ExploreStacks/PullRequestsScreen";
import { ReleasesScreen } from "../screens/ExploreStacks/ReleasesScreen";
import { WatchersScreen } from "../screens/ExploreStacks/WatchersScreen";
import { StarsScreen } from "../screens/ExploreStacks/StarsScreen";

const HomeStack = createStackNavigator({
  Home: HomeScreen,
  MyRepoDetail: MyRepoDetailScreen,
  MyForks: MyForksScreen,
  MyIssues: MyIssuesScreen,
  MyPullRequests: MyPullRequestsScreen,
  MyReleases: MyReleasesScreen,
  MyWatchers: MyWatchersScreen,
  MyStars: MyStarsScreen
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? 'ios-home'
          : 'md-home'
      }
    />
  ),
};

const ExploreStack = createStackNavigator({
  Explore: ExploreScreen,
  RepoDetail: RepoDetailScreen,
  Forks: ForksScreen,
  Issues: IssuesScreen,
  PullRequests: PullRequestsScreen,
  Releases: ReleasesScreen,
  Watchers: WatchersScreen,
  Stars: StarsScreen
});
ExploreStack.navigationOptions = {
  tabBarLabel: 'Explore',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-search' : 'md-search' }
    />
  )
};

const AccountStack = createStackNavigator({
  Account: AccountScreen
});
AccountStack.navigationOptions = {
  tabBarLabel: 'Account',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-person' : 'md-person' }
    />
  )
};

export default createBottomTabNavigator({
  HomeStack,
  ExploreStack,
  AccountStack
});
