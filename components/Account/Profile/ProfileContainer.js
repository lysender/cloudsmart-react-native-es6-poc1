import React from 'react';
import * as SecureStore from 'expo-secure-store';
import { StyleSheet, View, Alert } from 'react-native';
import { connect } from "react-redux";
import { Button, Avatar, ListItem } from "react-native-elements";
import { setAccessToken, setCurrentUser } from "../../../store/actions";

class ProfileContainer extends React.Component {
  handleLogoutPrompt = () => {
    Alert.alert(
      'Logout',
      'Are you sure you want to logout?',
      [
        {
          text: 'Cancel',
          onPress: this.handleLogoutCancelled
        },
        {
          text: 'OK',
          onPress: this.handleLogout
        }
      ],
      { cancelable: false }
    )
  };

  handleLogoutCancelled = () => {
    // Do nothing...
  };

  handleLogout = () => {
    // Clear secure storage
    SecureStore.deleteItemAsync('accessToken').then(() => {
      this.props.setAccessToken(null);
      this.props.setCurrentUser(null);

      this.props.navigation.navigate('AuthLoading');
    });
  };

  render() {
    if (this.props.currentUser) {
      const fullName = this.props.currentUser.full_name ? this.props.currentUser.full_name : '[Noone]';
      return (
        <View style={styles.container}>
          <View style={styles.avatarContainer}>
            <Avatar rounded
                    source={{ uri: this.props.currentUser.avatar_url }}
                    size="large"/>
          </View>

          <View>
            <ListItem title={this.props.currentUser.username} subtitle="Username" bottomDivider />
            <ListItem title={this.props.currentUser.email} subtitle="Email" bottomDivider/>
            <ListItem title={fullName} subtitle="Full name" bottomDivider/>
          </View>

          <View style={styles.logoutContainer} >
            <Button title="Logout" onPress={this.handleLogoutPrompt} />
          </View>
        </View>
      );
    }

    return null;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15
  },
  avatarContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10
  },
  logoutContainer: {
    marginTop: 20
  },
  spinnerTextStyle: {
    color: '#FFF'
  }
});

const mapStateToProps = (state, ownProps) => ({
  currentUser: state.currentUser,
  accessToken: state.accessToken
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  setCurrentUser: user => dispatch(setCurrentUser(user)),
  setAccessToken: token => dispatch(setAccessToken(token))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileContainer);
