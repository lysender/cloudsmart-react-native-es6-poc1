import React from 'react';
import * as SecureStore from 'expo-secure-store';
import { ActivityIndicator, StatusBar, View } from 'react-native';
import { connect } from 'react-redux';

import { CustomAxios } from "../../utils/CustomAxios";
import { setCurrentUser, setAccessToken } from "../../store/actions";

class AuthLoadingContainer extends React.Component {
  componentDidMount() {
    this.initAuth();
  }

  initAuth = () => {
    SecureStore.getItemAsync('accessToken').then(token => {
      console.log('accessToken: ');
      console.log(token);
      if (token) {
        this.testToken(token);
      } else {
        // No token
        this.props.navigation.navigate('Auth');
      }
    });
  };

  testToken = (token) => {
    const headers = { 'Authorization': 'Bearer ' + token };
    CustomAxios.get('/user', { headers: headers }).then(res => {
      // Token is valid
      // Set current user and token into redux store/state
      this.props.setAccessToken(token);
      this.props.setCurrentUser(res.data);

      // Move to the app navigation screen
      this.props.navigation.navigate('App');
    }).catch(err => {
      // Token is invalid
      console.log(err.message);
      this.props.navigation.navigate('Auth');
    });
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  currentUser: state.currentUser
});

const mapDispatchToProps = (dispatch) => ({
  setCurrentUser: user => dispatch(setCurrentUser(user)),
  setAccessToken: token => dispatch(setAccessToken(token))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthLoadingContainer);
