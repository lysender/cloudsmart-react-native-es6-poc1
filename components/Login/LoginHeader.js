import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';

export const LoginHeader = (props) => {
  return (
    <View>
      <View style={styles.logoContainer}>
        <Image style={styles.logo} source={require('../../assets/images/gitea-lg.png')} />
      </View>

      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          Login to Gitea
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  titleContainer: {
    alignItems: 'center'
  },
  title: {
    fontSize: 26
  },
  logoContainer: {
    alignItems: 'center'
  },
  logo: {
    width: 100,
    height: 100
  }
});
