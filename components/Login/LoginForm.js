import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Input, Button } from "react-native-elements";

export const LoginForm = (props) => {
  return (
    <View>
      <View style={styles.inputContainer}>
        <Input placeholder="Username"
               value={props.username}
               onChangeText={props.onChangeUsername} />
        <Input placeholder="Password"
               value={props.password}
               onChangeText={props.onChangePassword}
               secureTextEntry={true}
               textContentType="password" />
      </View>

      <View style={styles.buttonContainer}>
        <Button title="Login"
                onPress={props.onLogin}
                loading={props.submitting}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  inputContainer: {
    marginHorizontal: 30
  },
  buttonContainer: {
    marginTop: 30,
    marginHorizontal: 40
  }
});
