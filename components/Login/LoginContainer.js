import React from 'react';
import * as SecureStore from 'expo-secure-store';
import { View, StyleSheet, Alert } from 'react-native';
import { connect } from "react-redux";
import { LoginForm } from "./LoginForm";
import { LoginHeader } from "./LoginHeader";
import { CustomAxios } from "../../utils/CustomAxios";
import { setAccessToken, setCurrentUser } from "../../store/actions";

class LoginContainer extends React.Component {
  state = {
    username: '',
    password: '',
    submitting: false
  };

  handleUsername = (value) => {
    this.setState({ username: value });
  };

  handlePassword = (value) => {
    this.setState({ password: value });
  };

  /**
   * Fetch profile based on credentials given
   * Get actual user name from profile result
   * Get access tokens and take the first token
   * If no tokens yet, create a new one
   * Save token in secure storage
   * Redirect to home screen
   */
  handleLogin = () => {
    if (!this.state.submitting) {
      this.setState({ submitting: true });
      this.fetchProfile();
    }
  };

  fetchProfile = () => {
    // Validate credentials by fetching current user profile
    const config = { auth: { username: this.state.username, password: this.state.password } };
    CustomAxios.get('/user', config).then(res => {
      // If it succeeds, fetch an api token to switch to
      this.fetchTokens(res.data, config);
    }).catch(err => {
      console.log(err.message);
      this.handleLoginFailed();
    });
  };

  fetchTokens = (user, config) => {
    // Fetch api tokens
    CustomAxios.get('/users/' + user.username + '/tokens', config).then(res => {
      const tokens = res.data;
      let token = null;

      if (tokens.length > 0) {
        token = this.fetchValidToken(tokens);
      }

      if (token) {
        // Take the first token and proceed to login success
        this.handleLoginSuccess(user, token);
      } else {
        // Create a new token instead
        this.createToken(user, config);
      }

    }).catch(err => {
      console.log(err.message);
      this.handleLoginFailed();
    });
  };

  /**
   * Token sha1 are not being returned by the API for some reason
   *
   * @param tokens
   * @returns {null}
   */
  fetchValidToken = (tokens) => {
    if (tokens) {
      tokens.forEach((v) => {
        if (v['sha1'] && v['sha1'] !== '') {
          return v['sha1'];
        }
      });
    }

    return null;
  };

  createToken = (user, config) => {
    const data = { name: 'ExpoAppAccessToken' };
    CustomAxios.post('/users/' + user.username + '/tokens', data, config).then(res => {
      this.handleLoginSuccess(user, res.data['sha1']);
    }).catch(err => {
      console.log(err.message);
      this.handleLoginFailed();
    });
  };

  handleLoginSuccess = (user, token) => {
    this.setState({ submitting: false}, () => {
      // Save token in secure storage
      SecureStore.setItemAsync('accessToken', token).then(() => {
        // Save auth and token into redux
        this.props.setAccessToken(token);
        this.props.setCurrentUser(user);

        // Redirect to main app
        this.props.navigation.navigate('App');
      }).catch(err => {
        console.log(err.message);
      });
    });
  };

  handleLoginFailed = () => {
    this.setState({ submitting: false }, () => {
      Alert.alert('Login Failed', 'Invalid username or password.');
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <LoginHeader/>
        <View style={styles.loginForm}>
          <LoginForm onLogin={this.handleLogin}
                     onChangeUsername={this.handleUsername}
                     onChangePassword={this.handlePassword}
                     username={this.state.username}
                     password={this.state.password}
                     submitting={this.state.submitting}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
    marginVertical: 20
  },
  loginForm: {
    marginTop: 50
  },
  spinnerTextStyle: {
    color: '#FFF'
  }
});

const mapStateToProps = (state) => ({
  currentUser: state.currentUser,
  accessToken: state.accessToken
});

const mapDispatchToProps = (dispatch) => ({
  setCurrentUser: user => dispatch(setCurrentUser(user)),
  setAccessToken: token => dispatch(setAccessToken(token))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginContainer);
