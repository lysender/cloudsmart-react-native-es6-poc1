import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { RepoList } from "../../Repos/RepoList/RepoList";
import { SearchBox} from "../../Repos/Search/SearchBox";
import { AppAxios } from "../../../utils/AppAxios";

export class SearchReposContainer extends React.Component {
  state = {
    keyword: '',
    repos: [],
    searching: false,
    searched: false
  };

  componentDidMount() {
    this.searchRepos();
  }

  handleChangeKeyword = (keyword) => {
    this.setState({ keyword: keyword, searched: true }, () => {
      this.searchRepos();
    });
  };

  handleViewRepo = (repo) => {
    this.props.navigation.navigate('RepoDetail', { id: repo.id });
  };

  searchRepos = () => {
    this.setState({ repos: [], searching: true });

    let params = {};
    if (this.state.keyword) {
      params['q'] = this.state.keyword;
    }
    if (this.props.uid) {
      params['uid'] = this.props.uid;
    }

    const config = { params: params };

    AppAxios.get('/repos/search', config).then(res => {
      const repos = Array().concat(res.data.data);
      this.setState({ repos: repos, searching: false });
    }).catch(err => {
      console.log(err);
      this.setState({ searching: false });
    });
  };

  render() {
    let noResults = null;
    if (!this.state.searching && this.state.searched && this.state.repos.length === 0) {
      noResults = (
        <Text style={styles.noResults}>No matching repositories found.</Text>
      );
    }

    return (
      <View style={styles.container}>
        <SearchBox keyword={this.state.keyword}
                   onChangeKeyword={this.handleChangeKeyword}
                   searching={this.state.searching} />
        <RepoList repos={this.state.repos} onViewRepo={this.handleViewRepo}/>
        {noResults}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  noResults: {
    marginTop: 20,
    marginHorizontal: 15
  }
});
