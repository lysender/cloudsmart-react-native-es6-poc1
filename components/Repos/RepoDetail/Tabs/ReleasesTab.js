import React from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native'
import { ListItem } from "react-native-elements";

export const ReleasesTab = (props) => {
  let list = null;
  if (props.releases && props.releases.length > 0) {
    list = (
      <View>
        {props.releases.map(release => (
          <ListItem key={release.id} title={release.title} />
        ))}
      </View>
    );
  } else {
    list = <Text>No releases yet.</Text>
  }
  return (
    <View style={styles.container}>
      {list}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 15
  }
});
