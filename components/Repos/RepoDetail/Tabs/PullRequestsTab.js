import React from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native'
import { ListItem } from "react-native-elements";

export const PullRequestsTab = (props) => {
  let list = null;
  if (props.pullRequests && props.pullRequests.length > 0) {
    list = (
      <View>
        {props.issues.map(pull => (
          <ListItem key={pull.id} title={pull.title} />
        ))}
      </View>
    );
  } else {
    list = <Text>No pull requests yet.</Text>
  }
  return (
    <View style={styles.container}>
      {list}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 15
  }
});
