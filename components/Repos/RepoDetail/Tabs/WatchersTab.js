import React from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native'

export const WatchersTab = (props) => {

  return (
    <View style={styles.container}>
      <Text>This is the watchers tab</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 15
  }
});
