import React from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native'

export const StarsTab = (props) => {

  return (
    <View style={styles.container}>
      <Text>This is the stars tab</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 15
  }
});
