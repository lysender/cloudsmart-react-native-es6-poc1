import React from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native'
import { ListItem } from "react-native-elements";

export const IssuesTab = (props) => {
  let list = null;
  if (props.issues && props.issues.length > 0) {
    list = (
      <View>
        {props.issues.map(issue => (
          <ListItem key={issue.id} title={issue.title}/>
        ))}
      </View>
    );
  } else {
    list = <Text>No issues yet.</Text>
  }
  return (
    <View style={styles.container}>
      {list}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
    marginHorizontal: 15
  }
});
