import React from 'react';
import { Platform, Text, View, StyleSheet, ScrollView, ActivityIndicator } from 'react-native';
import { RepoDetail } from "./RepoDetail";
import { connect } from "react-redux";
import { AppAxios } from "../../../utils/AppAxios";
import { createAxiosConfig } from "../../../utils/AppAxios";

class RepoDetailContainer extends React.Component {
  parentNavStack = null;

  state = {
    repo: null,
    issues: [],
    pullRequests: [],
    releases: [],
    loading: true
  };

  componentDidMount() {
    const config = createAxiosConfig(this.props);
    AppAxios.get('/repositories/' + this.props.navigation.getParam('id'), config).then(res => {
      this.setState({ repo: res.data, loading: false });

      this.loadIssues();
      this.loadPullRequests();
      this.loadReleases();
    }).catch(err => {
      console.log(err.message);
    });
  }

  loadReleases = () => {
    const config = createAxiosConfig(this.props);
    const url = '/repos/' + this.state.repo.owner.username + '/' + this.state.repo.name + '/releases';

    AppAxios.get(url, config).then(res => {
      this.setState({ releases: res.data });
    }).catch(err => {
      console.log(err.message);
    });
  };

  loadIssues = () => {
    const config = createAxiosConfig(this.props);
    const url = '/repos/' + this.state.repo.owner.username + '/' + this.state.repo.name + '/issues';

    AppAxios.get(url, config).then(res => {
      this.setState({ issues: res.data });
    }).catch(err => {
      console.log(err.message);
    });
  };

  loadPullRequests = () => {
    const config = createAxiosConfig(this.props);
    const url = '/repos/' + this.state.repo.owner.username + '/' + this.state.repo.name + '/pulls';

    AppAxios.get(url, config).then(res => {
      this.setState({ pullRequests: res.data });
    }).catch(err => {
      console.log(err.message);
    });
  };

  handleListItemSelection = (item) => {
    let nextPage = item;
    if (this.props.navigation.state && this.props.navigation.state.routeName && this.props.navigation.state.routeName === 'MyRepoDetail') {
      nextPage = 'My' + item;
    }

    console.log(this.parentNavStack);
    console.log(nextPage);

    this.props.navigation.navigate(nextPage, { repo: this.state.repo });
  };

  render() {
    let loader = null;
    if (this.state.loading) {
      loader = <ActivityIndicator/>;
    }

    let repoContent = null;
    if (this.state.repo) {
      repoContent = (
          <RepoDetail repo={this.state.repo}
                      issues={this.state.issues}
                      pullRequests={this.state.pullRequests}
                      releases={this.state.releases}
                      onListItemSelect={this.handleListItemSelection}/>
      );
    }

    return (
      <View style={styles.container}>
        <ScrollView>
          {loader}
          {repoContent}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
  }
});

const mapStateToProps = (state) => ({
  currentUser: state.currentUser,
  accessToken: state.accessToken
});

export default connect(
  mapStateToProps
)(RepoDetailContainer);
