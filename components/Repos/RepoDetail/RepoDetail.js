import React from 'react';
import { Text, View, StyleSheet } from 'react-native'
import { ListItem } from "react-native-elements";

export const RepoDetail = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>
          {props.repo.owner.username + ' / ' + props.repo.name}
        </Text>
      </View>

      <View style={styles.descContainer}>
        <Text style={styles.desc}>{props.repo.description}</Text>
      </View>

      <View style={styles.listContainer}>
        <ListItem leftIcon={{ name: 'menu', size: 20 }}
                  title="Watchers"
                  badge={{ status: 'primary', value: props.repo.watchers_count }}
                  rightIcon={{ name: 'chevron-right', size: 20 }}
                  onPress={() => props.onListItemSelect('Watchers')}
                  bottomDivider/>
        <ListItem leftIcon={{ name: 'menu', size: 20 }}
                  title="Stars"
                  badge={{ status: 'success', value: props.repo.stars_count }}
                  rightIcon={{ name: 'chevron-right', size: 20 }}
                  onPress={() => props.onListItemSelect('Stars')}
                  bottomDivider/>
        <ListItem leftIcon={{ name: 'menu', size: 20 }}
                  title="Forks"
                  badge={{ status: 'primary', value: props.repo.forks_count }}
                  rightIcon={{ name: 'chevron-right', size: 20 }}
                  onPress={() => props.onListItemSelect('Forks')}
                  bottomDivider/>
        <ListItem leftIcon={{ name: 'menu', size: 20 }}
                  title="Issues"
                  badge={{ status: 'error', value: props.issues.length }}
                  rightIcon={{ name: 'chevron-right', size: 20 }}
                  onPress={() => props.onListItemSelect('Issues')}
                  bottomDivider/>
        <ListItem leftIcon={{ name: 'menu', size: 20 }}
                  title="Pull Requests"
                  badge={{ status: 'primary', value: props.pullRequests.length }}
                  rightIcon={{ name: 'chevron-right', size: 20 }}
                  onPress={() => props.onListItemSelect('PullRequests')}
                  bottomDivider/>
        <ListItem leftIcon={{ name: 'menu', size: 20 }}
                  title="Releases"
                  badge={{ status: 'success', value: props.releases.length }}
                  rightIcon={{ name: 'chevron-right', size: 20 }}
                  onPress={() => props.onListItemSelect('Releases')}
                  bottomDivider/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15
  },
  titleContainer: {
    marginHorizontal: 15
  },
  title: {
    fontSize: 20
  },
  descContainer: {
    marginHorizontal: 15,
    marginTop: 20
  },
  desc: {
    fontSize: 16
  },
  listContainer: {
    marginTop: 20
  }
});
