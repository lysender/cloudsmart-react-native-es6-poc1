import React from 'react';
import { Platform, View, StyleSheet } from 'react-native';
import { Button, Input, SearchBar } from "react-native-elements";

export const SearchBox = (props) => {
  const platform = Platform.OS === 'ios' ? 'ios' : 'android';
  return (
    <View style={styles.container}>
        <SearchBar placeholder="Enter keyword...."
                   platform={platform}
                   value={props.keyword}
                   showLoading={props.searching}
                   onChangeText={props.onChangeKeyword}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 15
  }
});
