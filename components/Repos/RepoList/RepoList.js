import React from 'react';
import { View } from 'react-native'
import { RepoItem } from "./RepoItem";

export const RepoList = (props) => {
  let repos = [];
  if (props.repos) {
    repos = props.repos.map((item) => {
      return (
        <RepoItem key={item.id} item={item} onViewRepo={props.onViewRepo} />
      );
    })
  }

  return (
    <View>
      {repos}
    </View>
  );
};
