import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native'
import { Card } from "react-native-elements";

export const RepoItem = (props) => {
  let desc = null;
  if (props.item.description) {
    desc = (
      <Text>{props.item.description}</Text>
    );
  }
  if (props.item) {
    let title = props.item.owner.username + ' / ' + props.item.name;
    return (
      <TouchableOpacity onPress={() => props.onViewRepo(props.item)}>
        <Card style={styles.container} title={title}>
          {desc}
          <Text>Updated last: {props.item.updated_at}</Text>
        </Card>
      </TouchableOpacity>
    );
  }

  return null;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 15,
  },
  repoTitle: {
    fontSize: 18
  }
});
