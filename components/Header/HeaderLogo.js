import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';

export const HeaderLogo = (props) => {
  return (
    <Image source={require('../../assets/images/gitea-sm.png')} style={styles.image} />
  );
};

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50
  }
});
