import React from 'react';
import { StyleSheet, Text } from 'react-native';

export const HeaderTitle = (props) => {
  return (
    <Text style={styles.title}>Gitea</Text>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    color: '#fff'
  }
});
