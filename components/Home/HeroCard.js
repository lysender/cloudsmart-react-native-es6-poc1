import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Icon } from "react-native-elements";

export const HeroCard = (props) => {
  return (
    <View style={styles.cardContainer}>
      <View style={styles.cardHeader}>
        <Icon name={props.icon} size={20 } style={styles.cardIcon} />
        <Text style={styles.cardTitle}>{props.title}</Text>
      </View>
      <View>
        <Text style={styles.cardText}>{props.description}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 20,
    marginHorizontal: 30
  },
  cardHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  cardIcon: {
    color: '#5aa509',
    marginRight: 10
  },
  cardTitle: {
    fontSize: 20,
  },
  cardText: {
    marginTop: 5,
    fontSize: 14
  }
});
