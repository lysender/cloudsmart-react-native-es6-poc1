import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { SearchBox} from "../../Repos/Search/SearchBox";
import { AppAxios } from "../../../utils/AppAxios";
import { RepoList } from "../../Repos/RepoList/RepoList";
import { connect } from "react-redux";

class MyReposContainer extends React.Component {
  state = {
    keyword: '',
    repos: [],
    searching: false,
    searched: false
  };

  componentDidMount() {
    this.searchRepos();
  }

  handleChangeKeyword = (keyword) => {
    this.setState({ keyword: keyword, searched: true }, () => {
      this.searchRepos();
    });
  };

  handleViewRepo = (repo) => {
    this.props.navigation.navigate('MyRepoDetail', { id: repo.id });
  };

  createAxiosHeaders = () => {
    return { Authorization: this.props.accessToken };
  };

  searchRepos = () => {
    this.setState({ repos: [], searching: true });

    let params = {};
    if (this.state.keyword) {
      params['q'] = this.state.keyword;
    }

    if (this.props.currentUser) {
      params['uid'] = this.props.currentUser.id;
    }

    const config = { params: params, headers: this.createAxiosHeaders() };

    AppAxios.get('/repos/search', config).then(res => {
      const repos = Array().concat(res.data.data);
      this.setState({ repos: repos, searching: false });
    }).catch(err => {
      console.log(err);
      this.setState({ searching: false });
    });
  };

  render() {
    let noResults = null;
    if (!this.state.searching && this.state.searched && this.state.repos.length === 0) {
      noResults = (
        <Text style={styles.noResults}>No matching repositories found.</Text>
      );
    }

    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Text style={styles.myRepos}>My Repositories</Text>
        </View>

        <SearchBox keyword={this.state.keyword}
                   onChangeKeyword={this.handleChangeKeyword}
                   searching={this.state.searching}/>
        <RepoList repos={this.state.repos} onViewRepo={this.handleViewRepo} />
        {noResults}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  title: {
    marginHorizontal: 15
  },
  myRepos: {
    fontSize: 20,
    marginBottom: 10,
  },
  noResults: {
    marginTop: 20,
    marginHorizontal: 15
  }
});

const mapStateToProps = (state) => ({
  currentUser: state.currentUser,
  accessToken: state.accessToken
});

export default connect(
  mapStateToProps
)(MyReposContainer);
