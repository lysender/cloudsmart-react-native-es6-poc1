export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SET_ACCESS_TOKEN = 'SET_ACCESS_TOKEN';

// Actions
export const setCurrentUser = (user) => ({
  type: SET_CURRENT_USER,
  user: user
});

export const setAccessToken = (token) => ({
  type: SET_ACCESS_TOKEN,
  token: token
});
