import { combineReducers, createStore} from "redux";
import { currentUser, accessToken } from "./reducers";

const reducer = combineReducers({ currentUser, accessToken });
const store = createStore(reducer);

export default store;
