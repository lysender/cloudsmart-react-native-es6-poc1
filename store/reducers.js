import { SET_ACCESS_TOKEN, SET_CURRENT_USER } from "./actions";

// Reducers
export const currentUser = (state = null, action) => {
  if (action.type === SET_CURRENT_USER) {
    return action.user;
  } else {
    return state;
  }
};

export const accessToken = (state = null, action) => {
  if (action.type === SET_ACCESS_TOKEN) {
    return action.token;
  } else {
    return state;
  }
};
