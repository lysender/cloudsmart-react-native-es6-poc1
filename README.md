[![CircleCI](https://circleci.com/bb/lysender/cloudsmart-react-native-es6-poc1.svg?style=svg)](https://circleci.com/bb/lysender/cloudsmart-react-native-es6-poc1)

# React Native ES6 POC 1

* [React](https://reactjs.org/)
* [React Native](https://facebook.github.io/react-native/)
* [Expo](https://expo.io/)

## React Native + Expo Review

### Technologies Used

* [React Native](https://facebook.github.io/react-native/)
  * React Native is just like React, but uses native components instead of web components
    as building blocks.
  * Codes are written in JavaScript to describe and call native components and 
    native APIs.
* [React Native Elements](https://react-native-training.github.io/react-native-elements/)
  * UI Toolkit for React Native
* [React Navigation](https://reactnavigation.org/)
  * Navigation workflow for React Native
* [Expo](https://expo.io/)
  * A set of tools, libraries and services to help build React Native iOS and Android apps
    faster. 
  * Expo is like Ruby on rails for React Native
  * Has two workflows:
    * Managed workflow
      * Write code in JavaScript then the Expo SDK will provide access to device capabilities
      * Allows using Expo services to test app locally through simulator or through actual
        device via the Expo client app
      * Allows using Expo services to publish app so that App updates wont go through
        AppStore or Google Play Store
        * Exceptions are native app specific configurations like App icon, splash screen
          etc, which requires manual upload to AppStore/Google Play Store 
    * Bare workflow
      * Does not use Expo services
      * Builds and deployments are done just like normal native app development
      * Allows full control over the iOS and Android projects
  * For managed workflows
    * They just announced a paid subscription service for priority builds
    * Managed apps builds are done on Expo servers 

### Pros

* Language
  * Uses JavaScript language by default
    * Using the modern JavaScript syntax (ES6) is encouraged
  * TypeScript can be used too - waiting for SDK33 release
    * Expo does not provide full support for TypeScript but support is under development
    * TypeScript can still work but when mixed with other libraries, integration
      is not that smooth
* Performance
  * Near native performance
  * Didn't notice any performance issues on the POC app, most slow
    activities are related to HTTP requests
* UX
  * Basic native elements are supported out of the box
  * Custom designs for elements can be achieved using styling (like CSS)
* Framework feature completeness
  * Basic components and APIs are available

### Cons

* Language
  * Still waiting for the full TypeScript support
  * Waiting for Expo team to release full TypeScript support and definitions for their modules
* Performance
  * Native apps are still faster due to the JS bridge to native components and APIs
* UX
  * Some UI element libraries are buggy (like Native Base), used React Native Elements instead
  * UI elements not provided by the stock library needs custom/manually written design
    in order to achieve the custom look (like writing CSS)
* Framework feature completeness
  * [Why not Expo?](https://docs.expo.io/versions/v33.0.0/introduction/why-not-expo/)
  * Not all native app APIs are supported
  * No bluetooth
  * No Apple Pay/Google Pay integration support
  * Background code support is limited
  * Installed app minimum size is large due to APIs included whether we use it or not
  * Limited support to older versions of iOS/Android
* Feedback

## Feedback

* Local development is done easily with tools provided by default by React Native
* Distributing the apps for testing is also easily done using Expo client (for managed apps)
* Building the native app for submission to App Store is also done using Expo services
* Note: Expo just announced a paid service for priority builds (usually for building the native
  app version for App Store submission)
* They might monetize other services in the future too.
