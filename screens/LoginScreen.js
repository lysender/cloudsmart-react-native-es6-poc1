import React from 'react';
import LoginContainer from '../components/Login/LoginContainer';

export class LoginScreen extends React.Component {
  render() {
    return (
      <LoginContainer navigation={this.props.navigation} />
    );
  }
}
