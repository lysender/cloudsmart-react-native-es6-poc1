import React from 'react';

import AuthLoadingContainer from '../components/Auth/AuthLoadingContainer';

export const AuthLoadingScreen = (props) => {
  // Render any loading content that you like here
  return (
    <AuthLoadingContainer navigation={props.navigation} />
  );
};
