import React from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';
import {SearchReposContainer} from "../components/Explore/SearchRepos/SearchReposContainer";

export class ExploreScreen extends React.Component {
  static navigationOptions = {
    title: 'Explore',
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <SearchReposContainer navigation={this.props.navigation} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
