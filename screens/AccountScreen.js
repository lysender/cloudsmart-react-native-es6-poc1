import React from 'react';
import { ScrollView, StyleSheet, Text } from 'react-native';
import ProfileContainer from '../components/Account/Profile/ProfileContainer';

export class AccountScreen extends React.Component {
  static navigationOptions = {
    title: 'Account',
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <ProfileContainer navigation={this.props.navigation} />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
