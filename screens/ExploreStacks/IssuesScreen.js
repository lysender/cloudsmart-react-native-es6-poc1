import React from 'react';
import { IssuesTab } from "../../components/Repos/RepoDetail/Tabs/IssuesTab";

export class IssuesScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo Issues',
  };

  render() {
    return (
      <IssuesTab navigation={this.props.navigation} />
    );
  }
}
