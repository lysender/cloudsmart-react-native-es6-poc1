import React from 'react';
import { PullRequestsTab } from "../../components/Repos/RepoDetail/Tabs/PullRequestsTab";

export class PullRequestsScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo Pull Requests',
  };

  render() {
    return (
      <PullRequestsTab navigation={this.props.navigation} />
    );
  }
}
