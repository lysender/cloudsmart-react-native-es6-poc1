import React from 'react';
import { WatchersTab } from "../../components/Repos/RepoDetail/Tabs/WatchersTab";

export class WatchersScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo Watchers',
  };

  render() {
    return (
      <WatchersTab navigation={this.props.navigation} />
    );
  }
}
