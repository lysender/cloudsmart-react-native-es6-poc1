import React from 'react';
import { StarsTab } from "../../components/Repos/RepoDetail/Tabs/StarsTab";

export class StarsScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo Stars',
  };

  render() {
    return (
      <StarsTab navigation={this.props.navigation} />
    );
  }
}
