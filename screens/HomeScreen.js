import React from 'react';
import { Platform, ScrollView, StyleSheet, View } from 'react-native';
import { Header } from "react-native-elements";
import MyReposContainer from "../components/Home/MyRepos/MyReposContainer";
import { HeaderLogo } from "../components/Header/HeaderLogo";
import { HeaderTitle } from "../components/Header/HeaderTitle";

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    return (
      <View style={styles.container}>
        <Header placement="left"
                leftComponent={HeaderLogo}
                centerComponent={HeaderTitle} />

        <View style={styles.flexi}>
          <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
            <MyReposContainer navigation={this.props.navigation} />
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  flexi: {
    flex: 1
  },
  contentContainer: {
    paddingTop: 10,
  }
});
