import React from 'react';
import RepoDetailContainer from "../components/Repos/RepoDetail/RepoDetailContainer";

export class RepoDetailScreen extends React.Component {
  static navigationOptions = {
    title: 'Repository',
  };

  render() {
    return (
      <RepoDetailContainer navigation={this.props.navigation} />
    );
  }
}
