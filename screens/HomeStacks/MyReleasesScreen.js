import React from 'react';
import { ReleasesTab } from "../../components/Repos/RepoDetail/Tabs/ReleasesTab";

export class MyReleasesScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo Releases',
  };

  render() {
    return (
      <ReleasesTab navigation={this.props.navigation} />
    );
  }
}
