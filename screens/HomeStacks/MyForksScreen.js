import React from 'react';
import { ForksTab } from "../../components/Repos/RepoDetail/Tabs/ForksTab";

export class MyForksScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo Forks',
  };

  render() {
    return (
      <ForksTab navigation={this.props.navigation} />
    );
  }
}
