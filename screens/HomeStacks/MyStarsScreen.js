import React from 'react';
import { StarsTab } from "../../components/Repos/RepoDetail/Tabs/StarsTab";

export class MyStarsScreen extends React.Component {
  static navigationOptions = {
    title: 'Repo Stars',
  };

  render() {
    return (
      <StarsTab navigation={this.props.navigation} />
    );
  }
}
