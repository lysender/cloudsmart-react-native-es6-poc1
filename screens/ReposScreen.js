import React from 'react';
import { ScrollView, StyleSheet, } from 'react-native';
import { SearchReposContainer } from "../components/Explore/SearchRepos/SearchReposContainer";

export default class ReposScreen extends React.Component {
  static navigationOptions = {
    title: 'Repositories',
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <SearchReposContainer />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
});
