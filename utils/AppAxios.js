import axios from 'axios';

export const AppAxios = axios.create({
  baseURL: 'https://try.gitea.io/api/v1/'
});

export const createAxiosConfig = (props, config) => {
  // Handle empty config
  if (config === undefined || config === null) {
    config = {};
  }

  const newConfig = { ...config };

  // Inject Authorization token into headers when found
  if (props && props.accessToken) {
    // Also handle when headers is already present
    const headers = config.headers ? { ...config.headers } : {};
    headers['Authorization'] = 'Bearer ' + props.accessToken;
    newConfig['headers'] = headers;
  }

  return newConfig;
};
