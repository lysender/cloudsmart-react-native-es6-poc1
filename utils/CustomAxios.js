import axios from 'axios';

export const CustomAxios = axios.create({
  baseURL: 'https://try.gitea.io/api/v1/'
});
