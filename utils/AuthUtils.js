import Base64 from "Base64";

export class AuthUtils {
  static encodeCredentials = (username, password) => {
    return Base64.btoa(username + ':' + password);
  };

  static decodeCredentials = (encoded) => {
    const cred = { username: null, password: null };
    const str = Base64.atob(encoded);
    if (str) {
      const chunks = str.split(':');
      if (chunks.length === 2) {
        cred.username = chunks[0];
        cred.password = chunks[1];
      }
    }

    return cred;
  };
}
